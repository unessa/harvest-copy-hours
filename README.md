# Harvest copy hours

This project is used to copy harvest ours from source oranization to target organization.
Currently copying hours is linked to single task in a project.

Generating local config (must be done before anything else): `npm run generate-config`
Copy hours from target project to target project: `npm run copy`
Show current plus hours from source organization: `npm run saldo`
Copy time entries from one account to another: `npm run copy`
To create recurring time entry use: `npm run recurring-time-entry`


# Config file

Run following command to gonerate config file

```bash
npm run generate-config -- \
  --output="<your-config-file.json>" \
  --access-token="YOUR_DEVELOPER_TOKEN" \
  --source-harvest-account-id=1 \
  --source-project-name="project1" \
  --source-task-name="task1" \
  --target-harvest-account-id=2 \
  --target-project-name="project2" \
  --target-task-name="task2" \
  --saldo-from-date="2018-01-01" \
  --saldo-hours-per-day=8
```

# Testing

To run tests `npm test`
To run test coverage `npm test -- --coverage`
