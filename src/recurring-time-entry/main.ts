import * as colors from 'colors/safe'
import { loadAppConfig, IProjectConfig } from '../config'
import { configureLogger, LogLevel } from '../utils/logger'
import { readCommandLineArgs, getUsageGuide } from './command-line-args'
import { createRecurringTimeEntry } from './recurring-time-entry'

const logger = configureLogger(LogLevel.Verbose)

async function main(): Promise<void> {
  try {
      const options = readCommandLineArgs()
      logger.debug(`Options`, options)

      if (options.help) {
      logger.message(getUsageGuide())
      return
      }

      const appConfig = loadAppConfig(logger, options.config)

      // TODO 22.01.2019 nviik - App config should be refactored to
      // have some common section which all different scripts can use...
      const projectConfig: IProjectConfig = {
        harvestAccountId: appConfig.sourceProject.harvestAccountId,
        projectName: '',
        projectId: options.projectId,
        taskName: '',
        taskId: options.taskId,
        userId: appConfig.sourceProject.userId
      }

      if (!projectConfig.projectId || !projectConfig.taskId ||
          !projectConfig.userId || !projectConfig.harvestAccountId) {
        logger.warning(`Invalid project config provided`, projectConfig)
        return
      }

      if (options.dry) {
        logger.message(colors.red(`RUNNING IN DRY MODE`))
      }

      await createRecurringTimeEntry(
        logger,
        appConfig.accessToken,
        projectConfig,
        options.from,
        options.to,
        options.interval,
        options.hours,
        options.notes,
        options.dry
      )
  } catch (err) {
      logger.error(err.message)
  }
}

main()
