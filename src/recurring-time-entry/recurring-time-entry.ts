import * as moment from 'moment'
import * as colors from 'colors/safe'
import { IProjectConfig } from '../config'
import { IConsoleLogger } from '../utils/logger'
import { createTimeEntryRequest } from '../harvest-api'

const WEEKLY_INTERVAL = 'weekly'
const AvailableIntervals = new Set([WEEKLY_INTERVAL])

export async function createRecurringTimeEntry(
  logger: IConsoleLogger,
  accessToken: string,
  projectConfig: IProjectConfig,
  from: string,
  to: string,
  interval: string,
  hours: number,
  notes: string,
  isDryRun: boolean
): Promise<void> {
  if (!AvailableIntervals.has(interval)) {
    logger.error(`Invalid interval given: ${interval}`)
    return
  }

  if (!hours) {
    logger.error(`Hours must be provided`)
    return
  }

  const dates = getRecurringTimeEntryDates(logger, from, to, interval)

  await createRecurringTimeEntries(
    logger,
    accessToken,
    projectConfig,
    dates,
    hours,
    notes,
    isDryRun
  )
}

export function getRecurringTimeEntryDates(
  logger: IConsoleLogger,
  from: string,
  to: string,
  interval: string
): Date[] {
  const startDate = moment(from)
  const endDate = moment(to)

  logger.verbose(`startDate: ${startDate.toISOString()}`)
  logger.verbose(`endDate: ${endDate.toISOString()}`)

  const dates: Date[] = []

  let currentDate = startDate

  while (currentDate < endDate) {
    dates.push(currentDate.toDate())

    if (interval === WEEKLY_INTERVAL) {
      currentDate = moment(currentDate).add(7, 'd')
    }
  }

  return dates
}

async function createRecurringTimeEntries(
  logger: IConsoleLogger,
  accessToken: string,
  projectConfig: IProjectConfig,
  dates: Date[],
  hours: number,
  notes: string,
  isDryRun: boolean
): Promise<void> {
  logger.message(`Creating time entry with information (hours: ${hours}, notes: ${notes})`)

  for (const date of dates) {
    logger.message(`${colors.green('Creating new entry')} ${date.toDateString()}`)

    if (!isDryRun) {
      await createTimeEntryRequest(
        logger,
        accessToken,
        projectConfig,
        date.toISOString(),
        hours,
        notes
      )
    }
  }
}
