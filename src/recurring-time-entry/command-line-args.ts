import * as commandLineArgs from 'command-line-args'
import * as commandLineUsage from 'command-line-usage'
import { DEFAULT_LOCAL_CONFIG_FILE } from '../config'

export interface IRecurringTimeEntryConfigArgs {
  help: boolean,
  config: string,
  dry: boolean,
  from: string,
  to: string,
  interval: string,
  projectId: number,
  taskId: number,
  hours: number,
  notes: string
}

const optionDefinitions = [
  {
    name: 'help',
    type: Boolean,
    description: 'Display this usage guide'
  },
  {
    name: 'config',
    type: String,
    defaultOption: true,
    defaultValue: DEFAULT_LOCAL_CONFIG_FILE,
    description: 'The config file to use'
  },
  {
    name: 'dry',
    type: Boolean,
    description: 'Simulate creating recurring time entry without doing actual API calls.'
  },
  {
    name: 'from',
    type: String,
    description: 'The date when recurring task is about to start.'
  },
  {
    name: 'to',
    type: String,
    description: 'The date until recurring task is going to last'
  },
  {
    name: 'interval',
    type: String,
    description: 'how often recurring task is going to happen. Supported values are: weekly'
  },
  {
    name: 'projectId',
    type: Number,
    description: 'project id where time entry should be created'
  },
  {
    name: 'taskId',
    type: Number,
    description: 'task id where time entry should be created'
  },
  {
    name: 'hours',
    type: Number,
    description: 'number of hours spent on the task'
  },
  {
    name: 'notes',
    type: String,
    defaultValue: '',
    description: '(optional) notes for the task'
  }
]

export function readCommandLineArgs(): IRecurringTimeEntryConfigArgs {
  return commandLineArgs(optionDefinitions) as any
}

export function getUsageGuide(): string {
  return commandLineUsage([
    {
      header: 'Recurring time entry',
      content: 'Script for creating recurring time entry'
    },
    {
      header: 'Options',
      optionList: optionDefinitions
    }
  ])
}
