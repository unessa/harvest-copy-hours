import { configureLogger, LogLevel } from '../utils/logger'
import { getRecurringTimeEntryDates } from './recurring-time-entry'

const logger = configureLogger(LogLevel.None)

describe(`Recurring time entry`, () => {
  describe(`when getRecurringTimeEntryDates is called`, () => {
    it(`should return correct dates when using weekly interval`, () => {
      const dates = getRecurringTimeEntryDates(
        logger,
        '2019-01-28T06:25:46.148Z',
        '2019-03-24T06:25:46.148Z',
        'weekly')

      expect(dates).toEqual([
        new Date('2019-01-28T06:25:46.148Z'),
        new Date('2019-02-04T06:25:46.148Z'),
        new Date('2019-02-11T06:25:46.148Z'),
        new Date('2019-02-18T06:25:46.148Z'),
        new Date('2019-02-25T06:25:46.148Z'),
        new Date('2019-03-04T06:25:46.148Z'),
        new Date('2019-03-11T06:25:46.148Z'),
        new Date('2019-03-18T06:25:46.148Z')
      ])
    })
  })
})
