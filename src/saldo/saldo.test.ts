import { ISaldoConfig } from '../config'
import { FixtureProvider } from '../utils/fixture-provider'
import { configureLogger, LogLevel } from '../utils/logger'
import { mockHarvestApiRequest, TEST_ACCESS_TOKEN } from '../utils/test-utils'
import { calculateCurrentSaldo, getSaldoDateRange, sortByDescendingDate } from './saldo'
import { ISaldoConfigArgs } from './command-line-args'
import { firstDayOfCurrentMonth, lastDayOfCurrentMonth } from 'utils/datetime'
import { IHarvestTimeEntry } from '../harvest-api'

const logger = configureLogger(LogLevel.None)
const fixtureProvider = new FixtureProvider()

describe(`Saldo calculator`, () => {
  describe(`when calculateCurrentSaldo is called`, () => {
    beforeAll(() => {
      mockHarvestApiRequest(fixtureProvider.HarvestAccountId())
        .get('/time_entries')
        .query({
          from: fixtureProvider.DateRange().startDate,
          to: fixtureProvider.DateRange().endDate,
          page: 1,
          per_page: 100
        })
        .reply(200, fixtureProvider.HarvestTimeEntryResponse())
    })

    it(`should calculate current saldo correctly`, async () => {
      const calculatedSaldo = await calculateCurrentSaldo(logger, TEST_ACCESS_TOKEN,
        fixtureProvider.ProjectConfig(), fixtureProvider.DateRange(), fixtureProvider.HoursPerDay(),
        fixtureProvider.SaldoProjectId(), fixtureProvider.SaldoTaskId())

      const expectedSaldo =
        fixtureProvider.TotalSpentHours() -
        (fixtureProvider.TimeEntriesCount() * fixtureProvider.HoursPerDay())

      expect(expectedSaldo).toEqual(calculatedSaldo)
    })
  })

  describe(`when sortByDescendingDate is called`, () => {
    const a: IHarvestTimeEntry = { ...fixtureProvider.HarvestTimeEntries()[0], spent_date: '2018-06-01' }
    const b: IHarvestTimeEntry = { ...fixtureProvider.HarvestTimeEntries()[1], spent_date: '2018-07-01' }

    it(`should return negative number if first time entry before second`, () => {
      const sortResult = sortByDescendingDate(a, b)
      expect(sortResult).toBeLessThan(0)
    })

    it(`should return positive number if first time entry after second`, () => {
      const sortResult = sortByDescendingDate(b, a)
      expect(sortResult).toBeGreaterThan(0)
    })

    it(`should return zero if same date`, () => {
      const sortResult = sortByDescendingDate(a, a)
      expect(sortResult).toEqual(0)
    })
  })

  describe(`when getSaldoDateRange is called`, () => {
    const options: ISaldoConfigArgs = {
      help: false,
      config: '',
      from: '2017-01-01',
      to: '2017-01-31'
    }

    const saldoConfig: ISaldoConfig = {
      fromDate: fixtureProvider.DateRange().startDate,
      toDate: fixtureProvider.DateRange().endDate,
      hoursPerDay: fixtureProvider.HoursPerDay(),
      saldoProjectId: fixtureProvider.SaldoProjectId(),
      saldoTaskId: fixtureProvider.SaldoTaskId()
    }

    const emptyOptions = { ...options, from: '', to: '' }
    const emptyConfig = { ...saldoConfig, fromDate: '', toDate: '' }

    it(`should use dates from options`, () => {
      const dateRange = getSaldoDateRange(logger, options, saldoConfig)
      expect(dateRange).toEqual({
        startDate: new Date(options.from).toISOString(),
        endDate: new Date(options.to).toISOString()
      })
    })

    it(`should use dates from appConfig`, () => {
      const dateRange = getSaldoDateRange(logger, emptyOptions, saldoConfig)
      expect(dateRange).toEqual(fixtureProvider.DateRange())
    })

    it(`should fallback to current month`, () => {
      const dateRange = getSaldoDateRange(logger, emptyOptions, emptyConfig)
      expect(dateRange).toEqual({
        startDate: firstDayOfCurrentMonth(),
        endDate: lastDayOfCurrentMonth()
      })
    })
  })
})
