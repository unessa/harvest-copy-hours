import { loadAppConfig } from '../config'
import { configureLogger, LogLevel } from '../utils/logger'
import { readCommandLineArgs, getUsageGuide } from './command-line-args'
import { getSaldoDateRange, calculateCurrentSaldo } from './saldo'

const logger = configureLogger(LogLevel.Verbose)

async function main(): Promise<void> {
  try {
    const options = readCommandLineArgs()
    logger.debug(`Options`, options)

    if (options.help) {
      logger.message(getUsageGuide())
      return
    }

    const appConfig = loadAppConfig(logger, options['config'])
    const dateRange = getSaldoDateRange(logger, options, appConfig.saldo)

    logger.message(`Calculating saldo between dates ${dateRange.startDate} - ${dateRange.endDate}`)

    const currentSaldo = await calculateCurrentSaldo(
      logger, appConfig.accessToken, appConfig.sourceProject, dateRange,
      appConfig.saldo.hoursPerDay, appConfig.saldo.saldoProjectId, appConfig.saldo.saldoTaskId)
    logger.message(`Current saldo is ${currentSaldo}h`)
  } catch (err) {
    logger.error(err.message)
  }
}

main()
