import * as commandLineArgs from 'command-line-args'
import * as commandLineUsage from 'command-line-usage'
import { DEFAULT_LOCAL_CONFIG_FILE } from '../config'

export interface ISaldoConfigArgs {
  help: boolean,
  config: string,
  from: string,
  to: string
}

const optionDefinitions = [
  {
    name: 'help',
    type: Boolean,
    description: 'Display this usage guide'
  },
  {
    name: 'config',
    type: String,
    defaultOption: true,
    defaultValue: DEFAULT_LOCAL_CONFIG_FILE,
    description: 'The config file name'
  },
  {
    name: 'from',
    type: String,
    description: 'The date where to start calculting current saldo. ISO formatted string.'
  },
  {
    name: 'to',
    type: String,
    description: 'The date where to stop calculting current saldo. ISO formatted string.'
  }
]

export function readCommandLineArgs(): ISaldoConfigArgs {
  return commandLineArgs(optionDefinitions) as any
}

export function getUsageGuide(): string {
  return commandLineUsage([
    {
      header: 'Current saldo',
      content: 'Script for checking current status of saldo hours.'
    },
    {
      header: 'Options',
      optionList: optionDefinitions
    }
  ])
}
