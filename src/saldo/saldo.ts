import { ISaldoConfig, IProjectConfig } from '../config'
import { IConsoleLogger } from '../utils/logger'
import { getAllTimeEntries, IHarvestTimeEntry } from '../harvest-api'
import { ISODateRange, firstDayOfCurrentMonth, lastDayOfCurrentMonth } from '../utils/datetime'
import { ISaldoConfigArgs } from './command-line-args'

export async function calculateCurrentSaldo(
  logger: IConsoleLogger,
  accessToken: string,
  project: IProjectConfig,
  dateRange: ISODateRange,
  hoursPerDay: number,
  saldoProjectId: number,
  saldoTaskId: number
): Promise<number> {
  const timeEntries = await getAllTimeEntries(logger, accessToken, project.harvestAccountId, dateRange)
  logger.verbose(`number of time entries: ${timeEntries.length}`)

  const reportedHours = timeEntries
  .sort(sortByDescendingDate)
  .reduce((sum, timeEntry) => {
    if (isSaldoTimeEntry(timeEntry, saldoProjectId, saldoTaskId)) {
      sum.saldoHours += timeEntry.hours
    } else {
      sum.totalHours += timeEntry.hours
    }
    return sum
  }, { saldoHours: 0, totalHours: 0 })

  const uniqueDates = new Set(timeEntries.map(timeEntry => timeEntry.spent_date))
  const totalHoursInDateRange = uniqueDates.size * hoursPerDay
  const currentSaldo = reportedHours.totalHours - totalHoursInDateRange

  logger.verbose(`Total number of working hours in given period`, totalHoursInDateRange)
  logger.verbose(`User reported hours`, reportedHours)

  return currentSaldo
}

function isSaldoTimeEntry(
  timeEntry: IHarvestTimeEntry,
  saldoProjectId: number,
  saldoTaskId: number
): boolean {
  return timeEntry.project.id === saldoProjectId &&
    timeEntry.task.id === saldoTaskId
}

// For debugging purporse time entries are sorted in chronocical order. Makes easier to read logs.
export function sortByDescendingDate(
  a: IHarvestTimeEntry,
  b: IHarvestTimeEntry
): number {
  const at = new Date(a.spent_date).getTime()
  const bt = new Date(b.spent_date).getTime()
  return at - bt
}

export function getSaldoDateRange(
  logger: IConsoleLogger,
  options: ISaldoConfigArgs,
  saldoConfig: ISaldoConfig
): ISODateRange {
  const dateRange: ISODateRange = {
    startDate: new Date(options.from || saldoConfig.fromDate || firstDayOfCurrentMonth()).toISOString(),
    endDate: new Date(options.to || saldoConfig.toDate || lastDayOfCurrentMonth()).toISOString()
  }
  logger.debug(`using date range`, dateRange)
  return dateRange
}
