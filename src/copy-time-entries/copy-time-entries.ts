import * as moment from 'moment'
import * as colors from 'colors/safe'
import { IProjectConfig } from '../config'
import { IConsoleLogger } from '../utils/logger'
import { getCurrentMonthDateRange } from '../utils/datetime'

import {
  getTimeEntriesRequest,
  createTimeEntryRequest,
  IHarvestTimeEntry
} from '../harvest-api'

export async function getTimeEntries(
  logger: IConsoleLogger,
  accessToken: string,
  project: IProjectConfig
): Promise<IHarvestTimeEntry[]> {
  const currentMonth = getCurrentMonthDateRange()
  const res = await getTimeEntriesRequest(logger, accessToken,
    project.harvestAccountId, currentMonth, project.projectId)
  return res.time_entries
}

export async function copyTimeEntries(
  logger: IConsoleLogger,
  accessToken: string,
  targetProject: IProjectConfig,
  sourceTimeEntries: IHarvestTimeEntry[],
  targetTimeEntries: IHarvestTimeEntry[],
  isDryRun: boolean
): Promise<void> {
  const sortedSourceTimeEntries = sortTimeEntriesByDate(sourceTimeEntries)
  const targetSpentDates = createSpentDaysMap(targetTimeEntries)

  for (const timeEntry of sortedSourceTimeEntries) {
    const isReported = targetSpentDates.has(timeEntry.spent_date)

    if (isReported) {
      const spentHoursStr = colors.dim(`(${timeEntry.hours}h)`)
      logger.message(`${timeEntry.spent_date} ${colors.yellow('REPORTED')} ${spentHoursStr}`)
    } else {
      logger.message(`${timeEntry.spent_date} ${colors.green('COPYING')}`)

      if (!isDryRun) {
        await createTimeEntryRequest(
          logger,
          accessToken,
          targetProject,
          timeEntry.spent_date,
          timeEntry.hours,
          timeEntry.notes || ''
        )
      }
    }
  }
}

export function sortTimeEntriesByDate(
  timeEntries: IHarvestTimeEntry[]
): IHarvestTimeEntry[] {
  return timeEntries.sort((a, b) => moment(a.spent_date).unix() - moment(b.spent_date).unix())
}

export function createSpentDaysMap(
  timeEntries: IHarvestTimeEntry[]
): Map<string, IHarvestTimeEntry> {
  const mapped = timeEntries.map(t => [t.spent_date, t] as [string, IHarvestTimeEntry])
  return new Map<string, IHarvestTimeEntry>(mapped)
}
