import * as commandLineArgs from 'command-line-args'
import * as commandLineUsage from 'command-line-usage'
import { DEFAULT_LOCAL_CONFIG_FILE } from '../config'

export interface ICopyTimeEntriesConfigArgs {
  help: boolean,
  config: string,
  dry: boolean
}

const optionDefinitions = [
  {
    name: 'help',
    type: Boolean,
    description: 'Display this usage guide'
  },
  {
    name: 'config',
    type: String,
    defaultOption: true,
    defaultValue: DEFAULT_LOCAL_CONFIG_FILE,
    description: 'The config file to use'
  },
  {
    name: 'dry',
    type: Boolean,
    description: 'Simulate copying hours without doing actual copying'
  }
]

export function readCommandLineArgs(): ICopyTimeEntriesConfigArgs {
  return commandLineArgs(optionDefinitions) as any
}

export function getUsageGuide(): string {
  return commandLineUsage([
    {
      header: 'Copy time entries',
      content: 'Script for copying time entries from source task to target task'
    },
    {
      header: 'Options',
      optionList: optionDefinitions
    }
  ])
}
