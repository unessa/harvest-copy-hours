import * as colors from 'colors/safe'
import { loadAppConfig } from '../config'
import { configureLogger, LogLevel } from '../utils/logger'
import { readCommandLineArgs, getUsageGuide } from './command-line-args'
import { copyTimeEntries, getTimeEntries } from './copy-time-entries'

const logger = configureLogger(LogLevel.Verbose)

async function main(): Promise<void> {
  try {
    const options = readCommandLineArgs()

    if (options.help) {
      logger.message(getUsageGuide())
      return
    }

    if (options.dry) {
      logger.message(colors.red(`RUNNING IN DRY MODE`))
    }

    const appConfig = loadAppConfig(logger, options.config)
    const sourceTimeEntries = await getTimeEntries(logger, appConfig.accessToken, appConfig.sourceProject)

    if (sourceTimeEntries.length === 0) {
      // TODO 19.01.2019 nviik - Perhaps we should logger.warning here?
      logger.message(colors.yellow(`Source project doesn't have any reported time entries for this month`))
      return
    }

    const targetTimeEntries = await getTimeEntries(logger, appConfig.accessToken, appConfig.targetProject)
    await copyTimeEntries(
      logger,
      appConfig.accessToken,
      appConfig.targetProject,
      sourceTimeEntries,
      targetTimeEntries,
      options.dry
    )
  } catch (err) {
    logger.error(err)
  }
}

main()
