import * as _ from 'lodash'
import * as moment from 'moment'
import { FixtureProvider } from '../utils/fixture-provider'
import { configureLogger, LogLevel } from '../utils/logger'
import { mockHarvestApiRequest, TEST_ACCESS_TOKEN } from '../utils/test-utils'
import { getTimeEntries, sortTimeEntriesByDate, createSpentDaysMap, copyTimeEntries } from './copy-time-entries'

const logger = configureLogger(LogLevel.None)
const fixtureProvider = new FixtureProvider()

describe(`Copy Time Entries`, () => {
  describe(`getTimeEntries`, () => {
    beforeAll(() => {
      mockHarvestApiRequest(fixtureProvider.HarvestAccountId())
        .get('/time_entries')
        .query({
          from: fixtureProvider.StartDate(),
          to: fixtureProvider.EndDate(),
          page: 1,
          per_page: 100,
          project_id: fixtureProvider.ProjectId()
        })
        .reply(200, fixtureProvider.HarvestTimeEntryResponse())
    })

    it(`should fetch time entries for project`, async () => {
      const timeEntries = await getTimeEntries(logger, TEST_ACCESS_TOKEN, fixtureProvider.ProjectConfig())
      expect(timeEntries).toEqual(fixtureProvider.HarvestTimeEntries())
    })
  })

  describe(`sortTimeEntriesByDate`, () => {
    it(`should sort time entries by date`, () => {
      const timeEntries = _.shuffle(fixtureProvider.HarvestTimeEntries())
      const sorted = sortTimeEntriesByDate(timeEntries)
      let startDate = sorted[0].spent_date

      for (let i = 1; i < sorted.length; ++i) {
        const isAfter = moment(sorted[i].spent_date).isAfter(moment(startDate))
        expect(isAfter).toBeTruthy()
        startDate = sorted[i].spent_date
      }
    })
  })

  describe(`createSpentDaysMap`, () => {
    it(`should create Map from given harvest time entries`, () => {
      const timeEntries = fixtureProvider.HarvestTimeEntries()
      const spentDays = createSpentDaysMap(timeEntries)

      for (const entry of timeEntries) {
        expect(spentDays.get(entry.spent_date)).toEqual(entry)
      }
    })
  })

  describe(`copyTimeEntries`, () => {
    const api = require('../harvest-api')
    const spy = jest.spyOn(api, 'createTimeEntryRequest')

    beforeEach(() => {
      fixtureProvider.HarvestTimeEntries().forEach((timeEntry: any) => {
        mockHarvestApiRequest(fixtureProvider.HarvestAccountId())
          .post('/time_entries')
          .query({
            user_id: fixtureProvider.HarvestUser().id,
            project_id: fixtureProvider.ProjectConfig().projectId,
            task_id: fixtureProvider.ProjectConfig().taskId,
            spent_date: timeEntry.spent_date,
            hours: timeEntry.hours,
            notes: timeEntry.notes
          })
          .reply(200)
      })
    })

    afterEach(() => {
      spy.mockReset()
    })

    it(`should copy time entries from source project to target project`, async () => {
      await copyTimeEntries(logger, TEST_ACCESS_TOKEN, fixtureProvider.ProjectConfig(),
        fixtureProvider.HarvestTimeEntries(), [], false)

      expect(spy).toHaveBeenCalledTimes(fixtureProvider.HarvestTimeEntries().length)
    })

    it(`should not do any modifiyng changes if dry mode is enabled`, async () => {
      await copyTimeEntries(logger, TEST_ACCESS_TOKEN, fixtureProvider.ProjectConfig(),
        fixtureProvider.HarvestTimeEntries(), [], true)

      expect(spy).not.toHaveBeenCalled()
    })
  })
})
