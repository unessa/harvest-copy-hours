import * as fs from 'fs'
import * as path from 'path'
import { IConsoleLogger } from './utils/logger'

export const HARVEST_API_URL = 'https://api.harvestapp.com/api/v2'
export const HARVEST_API_USER_AGENT = 'Copy harvest hours'
export const DEFAULT_LOCAL_CONFIG_FILE = 'local.config.json'

export interface IProjectConfig {
  harvestAccountId: number,
  projectName: string,
  projectId: number,
  taskName: string,
  taskId: number,
  userId: number
}

export interface ISaldoConfig {
  fromDate: string,
  toDate: string,
  hoursPerDay: number,
  saldoProjectId: number,
  saldoTaskId: number
}

export interface IAppConfig {
  accessToken: string,
  sourceProject: IProjectConfig,
  targetProject: IProjectConfig,
  saldo: ISaldoConfig
}

export function loadAppConfig(
  logger: IConsoleLogger,
  filename: string
): IAppConfig {
  const filePath = path.resolve(__dirname, '../', filename)
  logger.debug(`Loading config from file: ${filePath}`)

  if (!fs.existsSync(filePath)) {
    throw new Error(`Config file ${filePath} not found`)
  }

  const config: IAppConfig = require(`${filePath}`)

  const { accessToken, ...configToLog } = config
  logger.debug('config loaded:', configToLog)

  return config
}
