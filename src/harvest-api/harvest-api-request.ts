import * as request from 'request-promise'
import * as colors from 'colors/safe'
import { HARVEST_API_URL, HARVEST_API_USER_AGENT } from '../config'
import { IConsoleLogger } from '../utils/logger'

export async function harvestApiRequest(
  logger: IConsoleLogger,
  accessToken: string,
  harvestAccountId: number,
  method: 'GET' | 'POST',
  apiPath: string,
  query?: object
): Promise<request.RequestPromise> {
  const uri = `${HARVEST_API_URL}${apiPath}`

  try {
    const startTime = process.hrtime()

    const res = await request({
      json: true,
      method,
      uri,
      headers: {
        'Harvest-Account-ID': harvestAccountId,
        'Authorization': `Bearer ${accessToken}`,
        'User-Agent': HARVEST_API_USER_AGENT
      },
      useQuerystring: !!query,
      qs: query
    })

    const elapsed = (process.hrtime(startTime)[1] / 1000000).toFixed(2)
    logger.verbose(`HTTP/${method} ${elapsed}ms ${uri} (harvestAccountId=${harvestAccountId}) ${colors.green('OK')}`)
    logger.debug('response:', res)
    return res
  } catch (e) {
    throw new Error(`${colors.bold('HTTP/' + method)} ${uri} ${e}`)
  }
}
