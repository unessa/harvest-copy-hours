import { harvestApiRequest } from './harvest-api-request'
import { IConsoleLogger } from '../utils/logger'
import { IHarvestProjectAssigmentsResponse } from './harvest-api-types'

export async function getUSerProjectAssignmentsRequest(
  logger: IConsoleLogger,
  accessToken: string,
  harvestAccountId: number
): Promise<IHarvestProjectAssigmentsResponse> {
  return harvestApiRequest(
    logger,
    accessToken,
    harvestAccountId,
    'GET',
    '/users/me/project_assignments'
  )
}
