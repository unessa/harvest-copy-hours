import { IConsoleLogger } from '../utils/logger'
import { IProjectConfig } from '../config'
import { harvestApiRequest } from './harvest-api-request'
import { IHarvestTimeEntry, IHarvestTimeEntryResponse } from './harvest-api-types'
import { ISODateRange } from '../utils/datetime'

export async function createTimeEntryRequest(
  logger: IConsoleLogger,
  accessToken: string,
  project: IProjectConfig,
  spentDate: string,
  hours: number,
  notes: string
): Promise<IHarvestTimeEntry> {
  const query = {
    user_id: project.userId,
    project_id: project.projectId,
    task_id: project.taskId,
    spent_date: spentDate,
    hours,
    notes
  }

  return harvestApiRequest(
    logger,
    accessToken,
    project.harvestAccountId,
    'POST',
    '/time_entries',
    query
  )
}

export async function getTimeEntriesRequest(
  logger: IConsoleLogger,
  accessToken: string,
  harvestAccountId: number,
  timePeriod: ISODateRange,
  projectId?: number,
  page: number = 1,
  hitsPerPage: number = 100
): Promise<IHarvestTimeEntryResponse> {
  const query: any = {
    from: timePeriod.startDate,
    to: timePeriod.endDate,
    page,
    per_page: hitsPerPage
  }

  if (projectId) {
    query['project_id'] = projectId
  }

  return harvestApiRequest(
    logger,
    accessToken,
    harvestAccountId,
    'GET',
    '/time_entries',
    query
  )
}

export async function getAllTimeEntries(
  logger: IConsoleLogger,
  accessToken: string,
  harvestAccountId: number,
  timePeriod: ISODateRange,
  hitsPerPage: number = 100,
  projectId?: number
): Promise<IHarvestTimeEntry[]> {
  let timeEntries: IHarvestTimeEntry[] = []
  let currentPage = 0

  for (;;) {
    currentPage++
    const res = await getTimeEntriesRequest(logger, accessToken, harvestAccountId,
      timePeriod, undefined, currentPage, hitsPerPage)
    timeEntries = [...timeEntries, ...res.time_entries]

    if (res.page === res.total_pages) {
      break
    }
  }

  return timeEntries
}
