import { harvestApiRequest } from './harvest-api-request'
import { IConsoleLogger } from '../utils/logger'
import { IHarvestUser } from './harvest-api-types'

export async function getCurrentUserRequest(
  logger: IConsoleLogger,
  accessToken: string,
  harvestAccountId: number
): Promise<IHarvestUser> {
  return harvestApiRequest(
    logger,
    accessToken,
    harvestAccountId,
    'GET',
    '/users/me'
  )
}
