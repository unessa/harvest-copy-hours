export interface IHarvestApiResponse {
  per_page: number,
  total_pages: number,
  total_entries: number,
  next_page: string | null,
  previous_page: string | null,
  page: number,
  links: {
    first: string,
    next: string | null,
    previous: string | null,
    last: string
  }
}

export interface IHarvestTimeEntryResponse extends IHarvestApiResponse {
  time_entries: IHarvestTimeEntry[]
}

export interface IHarvestProjectAssigmentsResponse extends IHarvestApiResponse  {
  project_assignments: IHarvestProjectAssigment[]
}

export interface IHarvestProject {
  id: number,
  name: string,
  code: string
}

export interface IHarvestClient {
  id: number,
  name: string
}

export interface IHarvestTask {
  id: number,
  name: string
}

export interface IHarvestTimeEntry {
  id: number,
  spent_date: string,
  user: object,
  client: IHarvestClient,
  project: IHarvestProject,
  task: IHarvestTask,
  hours: number,
  notes: string | null,
  created_at: string,
  updated_at: string,
  is_locked: boolean,
  locked_reason: null,
  is_closed: false,
  is_billed: false,
  timer_started_at: string | null,
  ended_time: string | null,
  is_running: false,
  external_reference: null,
  billable: true,
  budgetet: false,
  billable_rate: null,
  cost_rate: null
}

export interface IHarvestProjectAssigment {
  id: number,
  is_project_manager: boolean,
  is_active: boolean,
  budget: null,
  created_at: string,
  updated_at: string,
  hourly_rate: null,
  project: IHarvestProject,
  client: IHarvestClient,
  task_assignments: IHarvestTaskAssigment[]
}

export interface IHarvestTaskAssigment {
  id: number,
  billable: boolean,
  is_active: boolean,
  created_at: string,
  updated_at: string,
  hourly_rate: null,
  budget: null,
  task: IHarvestTask
}

export interface IHarvestUser {
  id: number,
  first_name: string,
  last_name: string,
  email: string,
  telephone: string,
  timezone: string,
  has_access_to_all_future_projects: boolean,
  is_contractor: boolean,
  is_admin: boolean,
  is_project_manager: boolean,
  can_see_rates: boolean,
  can_create_projects: boolean,
  can_create_invoices: boolean,
  is_active: boolean,
  created_at: string,
  updated_at: string,
  default_hourly_rate: number,
  cost_rate: number,
  avatar_url: string
}
