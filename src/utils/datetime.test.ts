import * as moment from 'moment'

import {
  firstDayOfCurrentMonth,
  lastDayOfCurrentMonth,
  getCurrentMonthDateRange
} from './datetime'

const MOCKED_DATE = moment('2017-09-17T05:28:02.077Z').utcOffset(0).valueOf()

function mockGlobalDate(): void {
  Date.now = jest.fn(() => MOCKED_DATE)
}

describe('Datetime', () => {
  beforeAll(mockGlobalDate)

  describe('firstDayOfCurrentMonth', () => {
    it('should return correct ISO date', () => {
      const date = firstDayOfCurrentMonth()
      expect(date).toEqual('2017-09-01T00:00:00.000Z')
    })
  })

  describe('lastDayOfCurrentMonth', () => {
    it('should return correct ISO date', () => {
      const date = lastDayOfCurrentMonth()
      expect(date).toEqual('2017-09-30T23:59:59.999Z')
    })
  })

  describe('getCurrentMonthDateRange', () => {
    it('should return correct start and end dates', () => {
      const dateRange = getCurrentMonthDateRange()
      expect(dateRange).toEqual({
        startDate: '2017-09-01T00:00:00.000Z',
        endDate: '2017-09-30T23:59:59.999Z'
      })
    })
  })
})
