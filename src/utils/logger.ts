import * as colors from 'colors/safe'

export enum LogLevel {
  None = 0,
  Normal = 1,
  Verbose = 2,
  Debug = 3
}

export type LogMessageFunction = (message: string, ...args: any[]) => void

export interface IConsoleLogger {
  debug: LogMessageFunction,
  verbose: LogMessageFunction,
  message: LogMessageFunction,
  warning: LogMessageFunction,
  error: LogMessageFunction
}

export function configureLogger(
  logLevel: LogLevel = LogLevel.Normal
): IConsoleLogger {
  return {
    debug: (message: string, ...args: any[]) => { logDebugMessage(logLevel, message, ...args) },
    verbose: (message: string, ...args: any[]) => { logVerboseMessage(logLevel, message, ...args) },
    message: (message: string, ...args: any[]) => { logMessage(logLevel, message, ...args) },
    warning: (message: string, ...args: any[]) => { logWarningMessage(logLevel, message, ...args) },
    error: (message: string, ...args: any[]) => { logErrorMessage(logLevel, message, ...args) }
  }
}

function logDebugMessage(
  logLevel: LogLevel,
  message: string,
  ...args: any[]
): void {
  if (logLevel >= LogLevel.Debug) {
    transportToConsole(colors.dim(`DEBUG ${message}`), ...args)
  }
}

function logVerboseMessage(
  logLevel: LogLevel,
  message: string,
  ...args: any[]
): void {
  if (logLevel >= LogLevel.Verbose) {
    transportToConsole(colors.dim(message), ...args)
  }
}

function logMessage(
  logLevel: LogLevel,
  message: string,
  ...args: any[]
): void {
  if (logLevel >= LogLevel.Normal) {
    transportToConsole(message, ...args)
  }
}

function logWarningMessage(
  logLevel: LogLevel,
  message: string,
  ...args: any[]
): void {
  if (logLevel >= LogLevel.Normal) {
    const type = colors.yellow('WARNING')
    transportToConsole(`${type} ${message}`, ...args)
  }
}

function logErrorMessage(
  logLevel: LogLevel,
  message: string,
  ...args: any[]
): void {
  if (logLevel >= LogLevel.Normal) {
    const type = colors.red('ERROR')
    transportToConsole(`${type} ${message}`, ...args)
  }
}

function transportToConsole(
  message: string,
  ...args: any[]
): void {
  // tslint:disable-next-line
  console.log(message, ...args)
}
