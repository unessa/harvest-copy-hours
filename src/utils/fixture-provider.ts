import * as moment from 'moment'
import { IProjectConfig } from '../config'
import { ISODateRange } from '../utils/datetime'

import {
  randomId,
  randomDateBetweenRange,
  randomString,
  randomHoursPerDay,
  randomNumberBetween
} from './test-utils'

import {
  IHarvestApiResponse,
  IHarvestTimeEntryResponse,
  IHarvestTimeEntry,
  IHarvestProjectAssigmentsResponse,
  IHarvestProjectAssigment,
  IHarvestTaskAssigment,
  IHarvestUser
} from '../harvest-api'

export interface IFixtureProviderOptions {
  harvestAccountId?: number,
  userId?: number,
  clientId?: number,
  projectId?: number,
  projectName?: string,
  taskId?: number,
  taskName?: string,
  startDate?: string,
  endDate?: string,
  projectAssigmentsCount?: number,
  taskAssigmentsOnProjectCount?: number,
  timeEntriesCount?: number,
  hoursPerDay?: number,
  saldoProjectId?: number,
  saldoTaskId?: number
}

export class FixtureProvider {
  private harvestAccountId: number
  private clientId: number
  private projectId: number
  private projectName: string
  private taskId: number
  private taskName: string
  private startDate: string
  private endDate: string
  private taskAssigmentsOnProjectCount: number
  private timeEntriesCount: number
  private harvestUser: IHarvestUser
  private harvestProjectAssigments: IHarvestProjectAssigment[]
  private harvestTimeEntries: IHarvestTimeEntry[]
  private totalSpentHours: number = 0
  private totalSaldoHours: number = 0
  private hoursPerDay: number
  private saldoProjectId: number
  private saldoTaskId: number

  constructor(options?: IFixtureProviderOptions) {
    this.harvestAccountId = options && options.harvestAccountId || randomId()
    this.clientId = options && options.clientId || randomId()
    this.projectId = options && options.projectId || randomId()
    this.projectName = options && options.projectName || this.randomProjectName()
    this.taskId = options && options.taskId || randomId()
    this.taskName = options && options.taskName || this.randomTaskName()
    this.startDate = options && options.startDate || moment().utcOffset(0).startOf('month').toISOString()
    this.endDate = options && options.endDate || moment().utcOffset(0).endOf('month').toISOString()
    this.taskAssigmentsOnProjectCount = options && options.taskAssigmentsOnProjectCount || 10
    this.timeEntriesCount = options && options.timeEntriesCount || 30
    this.hoursPerDay = options && options.hoursPerDay || randomNumberBetween(4, 10)
    this.saldoProjectId = options && options.saldoProjectId || randomId()
    this.saldoTaskId = options && options.saldoTaskId || randomId()

    const userId = options && options.userId || randomId()
    const projectAssigmentsCount = options && options.projectAssigmentsCount || 30

    this.harvestUser = this.mockHarvestUser(userId)
    this.harvestProjectAssigments = this.mockHarvestProjectAssigments(projectAssigmentsCount)
    this.harvestTimeEntries = this.mockHarvestTimeEntries()
  }

  public ProjectConfig(): IProjectConfig {
    return {
      harvestAccountId: this.harvestAccountId,
      projectName: this.projectName,
      projectId: this.projectId,
      taskName: this.taskName,
      taskId: this.taskId,
      userId: this.harvestUser.id
    }
  }

  public StartDate(): string {
    return this.startDate
  }

  public EndDate(): string {
    return this.endDate
  }

  public DateRange(): ISODateRange {
    return {
      startDate: this.startDate,
      endDate: this.endDate
    }
  }

  public HarvestAccountId(): number {
    return this.harvestAccountId
  }

  public HarvestUser(): IHarvestUser {
    return this.harvestUser
  }

  public ProjectId(): number {
    return this.projectId
  }

  public ProjectName(): string {
    return this.projectName
  }

  public TaskId(): number {
    return this.taskId
  }

  public TaskName(): string {
    return this.taskName
  }

  public HarvestProjectAssigments(): IHarvestProjectAssigment[] {
    return this.harvestProjectAssigments
  }

  public HarvestProjectAssigmentsResponse(): IHarvestProjectAssigmentsResponse {
    const apiResponse = this.mockHarvestApiResponse(this.harvestProjectAssigments.length)
    return { ...apiResponse, project_assignments: this.harvestProjectAssigments }
  }

  public HarvestTimeEntries(): IHarvestTimeEntry[] {
    return this.harvestTimeEntries
  }

  public HarvestTimeEntryResponse(): IHarvestTimeEntryResponse {
    const apiResponse = this.mockHarvestApiResponse(this.harvestTimeEntries.length)
    return { ...apiResponse, time_entries: this.harvestTimeEntries }
  }

  public TotalSpentHours(): number {
    return this.totalSpentHours
  }

  public TotalSaldoHours(): number {
    return this.totalSaldoHours
  }

  public TimeEntriesCount(): number {
    return this.timeEntriesCount
  }

  public HoursPerDay(): number {
    return this.hoursPerDay
  }

  public SaldoProjectId(): number {
    return this.saldoProjectId
  }

  public SaldoTaskId(): number {
    return this.saldoTaskId
  }

  private mockHarvestTimeEntries(): IHarvestTimeEntry[] {
    const timeEntries: IHarvestTimeEntry[] = []

    for (let i = 0; i < this.timeEntriesCount; ++i) {
      const timEntry = this.mockHarvestTimeEntry()
      timeEntries.push(timEntry)
    }

    return timeEntries
  }

  private mockHarvestTimeEntry(): IHarvestTimeEntry {
    const entryDate = randomDateBetweenRange(this.startDate, this.endDate)
    const hours = randomHoursPerDay(this.hoursPerDay)

    // FIXME 07.02.2018 nviik - Add better random generator
    const isSaldoTimeEntry = !!randomNumberBetween(0, 5)

    if (isSaldoTimeEntry) {
      this.totalSaldoHours += hours
    } else {
      this.totalSpentHours += hours
    }

    return {
      id: randomId(),
      spent_date: entryDate,
      user: {
        id: this.harvestUser.id,
        name: `${this.harvestUser.first_name} ${this.harvestUser.last_name}`
      },
      client: {
        id: this.clientId,
        name: 'Client Company Name'
      },
      project: {
        id: isSaldoTimeEntry ? this.saldoProjectId : this.projectId,
        name: this.projectName,
        code: randomString()
      },
      task: {
        id: isSaldoTimeEntry ? this.saldoTaskId : this.taskId,
        name: 'Implementation'
      },
      hours,
      notes: `Note-${randomString()}`,
      created_at: entryDate,
      updated_at: entryDate,
      is_locked: false,
      locked_reason: null,
      is_closed: false,
      is_billed: false,
      timer_started_at: null,
      ended_time: null,
      is_running: false,
      external_reference: null,
      billable: true,
      budgetet: false,
      billable_rate: null,
      cost_rate: null
    }
  }

  private mockHarvestProjectAssigments(
    count: number
  ): IHarvestProjectAssigment[] {
    const projectAssigments: IHarvestProjectAssigment[] = []

    projectAssigments.push(this.mockHarvestProjectAssigment(this.projectId, this.projectName))

    for (let i = 0; i < count - 1; ++i) {
      const projectAssigment = this.mockHarvestProjectAssigment()
      projectAssigments.push(projectAssigment)
    }

    return projectAssigments
  }

  private mockHarvestProjectAssigment(
    projectId?: number,
    projectName?: string
  ): IHarvestProjectAssigment {
    return {
      id: randomId(),
      is_project_manager: false,
      is_active: false,
      budget: null,
      created_at: randomDateBetweenRange(this.startDate, this.endDate),
      updated_at: randomDateBetweenRange(this.startDate, this.endDate),
      hourly_rate: null,
      project: {
        id: projectId || randomId(),
        name: projectName || this.randomProjectName(),
        code: randomString()
      },
      client: {
        id: this.clientId,
        name: 'Client Company Name'
      },
      task_assignments: this.mockHarvestTaskAssigments(this.taskAssigmentsOnProjectCount)
    }
  }

  public mockHarvestTaskAssigments(
    count: number
  ): IHarvestTaskAssigment[] {
    const taskAssigments: IHarvestTaskAssigment[] = []

    taskAssigments.push(this.mockHarvestTaskAssigment(this.taskId, this.taskName))

    for (let i = 0; i < count - 1; ++i) {
      const taskAssigment = this.mockHarvestTaskAssigment()
      taskAssigments.push(taskAssigment)
    }

    return taskAssigments
  }

  public mockHarvestTaskAssigment(
    taskId?: number,
    taskName?: string
  ): IHarvestTaskAssigment {
    return {
      id: randomId(),
      billable: false,
      is_active: false,
      created_at: randomDateBetweenRange(this.startDate, this.endDate),
      updated_at: randomDateBetweenRange(this.startDate, this.endDate),
      hourly_rate: null,
      budget: null,
      task: {
        id: taskId || randomId(),
        name: taskName || this.randomTaskName()
      }
    }
  }

  private mockHarvestUser(
    userId: number
  ): IHarvestUser {
    return {
      id: userId,
      first_name: randomString(),
      last_name: randomString(),
      email: this.randomEmail(),
      telephone: '',
      timezone: '',
      has_access_to_all_future_projects: false,
      is_contractor: false,
      is_admin: false,
      is_project_manager: false,
      can_see_rates: false,
      can_create_projects: false,
      can_create_invoices: false,
      is_active: true,
      created_at: randomDateBetweenRange(this.startDate, this.endDate),
      updated_at: randomDateBetweenRange(this.startDate, this.endDate),
      default_hourly_rate: 10,
      cost_rate: 10,
      avatar_url: ''
    }
  }

  private mockHarvestApiResponse(
    totalEntries: number
  ): IHarvestApiResponse {
    return {
      per_page: totalEntries,
      total_pages: 1,
      total_entries: totalEntries,
      next_page: null,
      previous_page: null,
      page: 1,
      links: {
        first: '',
        next: null,
        previous: null,
        last: ''
      }
    }
  }

  private randomProjectName(): string {
    return `Project-${randomString()}`
  }

  private randomTaskName(): string {
    return `Task-${randomString()}`
  }

  private randomEmail(): string {
    return `${randomString()}@test.com`
  }
}
