import * as moment from 'moment'
import * as nock from 'nock'
import { HARVEST_API_URL, HARVEST_API_USER_AGENT } from '../config'

export const TEST_ACCESS_TOKEN = 'test-access-token'

export function randomId(): number {
  return Math.floor(Math.random() * 123243)
}

export function randomNumberBetween(
  start: number,
  end: number
): number {
  return Math.floor(Math.random() * (end - start) + start)
}

export function randomHoursPerDay(hoursPerDay: number) {
  const fullHours = hoursPerDay + randomNumberBetween(-2, 2)
  const partHours = Math.floor(Math.random() * 4) / 4
  return fullHours + partHours
}

export function randomDateBetweenRange(
  startDate: string,
  endDate: string
): string {
  const start = moment(startDate).unix()
  const end = moment(endDate).unix()
  return moment.unix(randomNumberBetween(start, end)).toISOString()
}

export function randomString(
  length: number = 5
): string {
  return Math.random().toString(36).substr(2, length)
}

export function mockHarvestApiRequest(
  harvestAccountId: number
): nock.Scope {
  return nock(HARVEST_API_URL, {
    reqheaders: {
      'Harvest-Account-ID': harvestAccountId.toString(),
      'Authorization': `Bearer ${TEST_ACCESS_TOKEN}`,
      'User-Agent': HARVEST_API_USER_AGENT
    }
  })
}
