import * as moment from 'moment'

export interface ISODateRange {
  startDate: string,
  endDate: string
}

export function firstDayOfCurrentMonth(): string {
  return moment().utcOffset(0).startOf('month').toISOString()
}

export function lastDayOfCurrentMonth(): string {
  return moment().utcOffset(0).endOf('month').toISOString()
}

export function getCurrentMonthDateRange(): ISODateRange {
  return {
    startDate: firstDayOfCurrentMonth(),
    endDate: lastDayOfCurrentMonth()
  }
}
