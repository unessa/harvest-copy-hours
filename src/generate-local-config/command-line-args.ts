import * as commandLineArgs from 'command-line-args'
import * as commandLineUsage from 'command-line-usage'
import { DEFAULT_LOCAL_CONFIG_FILE } from '../config'

export interface IGenerateLocalConfigArgs {
  help: boolean,
  output: string,
  'access-token': string,
  'source-harvest-account-id': number,
  'source-project-name': string,
  'source-task-name': string,
  'target-harvest-account-id': number,
  'target-project-name': string,
  'target-task-name': string,
  'saldo-from-date': string,
  'saldo-to-date': string,
  'saldo-hours-per-day': number,
  'saldo-hours-project-id': number,
  'saldo-hours-task-id': number
}

const optionDefinitions = [
  {
    name: 'help',
    type: Boolean,
    description: 'Display this usage guide'
  },
  {
    name: 'output',
    type: String,
    defaultOption: true,
    defaultValue: DEFAULT_LOCAL_CONFIG_FILE,
    description: 'The config file path'
  },
  {
    name: 'access-token',
    type: String,
    description: 'Harvest API access token'
  },
  {
    name: 'source-harvest-account-id',
    type: Number,
    description: 'Source organization id'
  },
  {
    name: 'source-project-name',
    type: String,
    description: 'The source project name'
  },
  {
    name: 'source-task-name',
    type: String,
    description: 'The task name which contains hours to be copied'
  },
  {
    name: 'target-harvest-account-id',
    type: Number,
    description: 'Target organization id'
  },
  {
    name: 'target-project-name',
    type: String,
    description: 'The proejct name'
  },
  {
    name: 'target-task-name',
    type: String,
    description: 'The task name where hours are copied'
  },
  {
    name: 'saldo-from-date',
    type: String,
    description: 'The date where to start calculting current saldo. ISO formatted string.'
  },
  {
    name: 'saldo-to-date',
    type: String,
    description: 'The date where to stop calculting current saldo. ISO formatted string.'
  },
  {
    name: 'saldo-hours-per-day',
    type: Number,
    defaultValue: 8,
    description: 'Number of hours in one work day'
  },
  {
    name: 'saldo-hours-project-id',
    type: Number,
    description: 'The saldo hours project id'
  },
  {
    name: 'saldo-hours-task-id',
    type: Number,
    description: 'The saldo hours task id'
  }
]

export function readCommandLineArgs(): IGenerateLocalConfigArgs {
  return commandLineArgs(optionDefinitions) as any
}

export function getUsageGuide(): string {
  return commandLineUsage([
    {
      header: 'Generate Local Config',
      content: 'Script for generating config file which can be used in other scripts like copying hours.'
    },
    {
      header: 'Options',
      optionList: optionDefinitions
    }
  ])
}
