import { IConsoleLogger } from '../utils/logger'
import { IProjectConfig } from '../config'

import {
  getUSerProjectAssignmentsRequest,
  getCurrentUserRequest,
  IHarvestProjectAssigment,
  IHarvestTaskAssigment
} from '../harvest-api'

export async function resolveLocalProjectConfig(
  logger: IConsoleLogger,
  accessToken: string,
  harvestAccountId: number,
  projectName: string,
  taskName: string
): Promise<IProjectConfig> {
  logger.message(`Resolving inormation for organization id ${harvestAccountId}`)
  logger.verbose(`details: projectName="${projectName}", taskName="${taskName}"`)

  const user = await getCurrentUserRequest(logger, accessToken, harvestAccountId)
  const projectAssigmentsResponse = await getUSerProjectAssignmentsRequest(logger, accessToken, harvestAccountId)
  const projectAssigment = getProjectAssigment(projectName, projectAssigmentsResponse.project_assignments)
  const taskAssigment = getTaskAssigment(taskName, projectAssigment.task_assignments)

  return {
    harvestAccountId,
    projectName,
    projectId: projectAssigment.project.id,
    taskName,
    taskId: taskAssigment.task.id,
    userId: user.id
  }
}

export function getProjectAssigment(
  projectName: string,
  projectAssigments: IHarvestProjectAssigment[]
): IHarvestProjectAssigment {
  const assigment = projectAssigments.find(({project}) => project.name === projectName)

  if (!assigment) {
    throw new Error(`No projects found for name ${projectName}`)
  }

  return assigment
}

export function getTaskAssigment(
  taskName: string,
  taskAssignments: IHarvestTaskAssigment[]
): IHarvestTaskAssigment {
  const assigment = taskAssignments.find(({task}) => task.name === taskName)

  if (!assigment) {
    throw new Error(`No tasks found for name ${taskName}`)
  }

  return assigment
}
