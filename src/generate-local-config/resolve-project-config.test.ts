import * as _ from 'lodash'
import { FixtureProvider } from '../utils/fixture-provider'
import { configureLogger, LogLevel } from '../utils/logger'
import { mockHarvestApiRequest, TEST_ACCESS_TOKEN } from '../utils/test-utils'
import { getProjectAssigment, getTaskAssigment, resolveLocalProjectConfig } from './resolve-project-config'

const logger = configureLogger(LogLevel.None)
const fixtureProvider = new FixtureProvider()

describe(`Generate Local Config`, () => {
  describe(`getProjectAssigment`, () => {
    const projectAssigments = _.shuffle(fixtureProvider.HarvestProjectAssigments())

    it(`should find correct project assigment`, () => {
      const projectName = fixtureProvider.ProjectName()
      const projectAssigment = getProjectAssigment(projectName, projectAssigments)
      expect(projectAssigment.project.name).toEqual(projectName)
    })

    it(`should throw error if project assigment is not found`, () => {
      const projectNotFoundName = 'project-not-found'
      const throws = () => getProjectAssigment(projectNotFoundName, projectAssigments)
      expect(throws).toThrowError(`No projects found for name ${projectNotFoundName}`)
    })
  })

  describe(`getTaskAssigment`, () => {
    const taskAssigments = _.shuffle(fixtureProvider.mockHarvestTaskAssigments(30))

    it(`should find correct task assigment`, () => {
      const taskName = fixtureProvider.TaskName()
      const taskAssigment = getTaskAssigment(taskName, taskAssigments)
      expect(taskAssigment.task.name).toEqual(taskName)
    })

    it(`should throw error if task assigment is not found`, () => {
      const taskNotFoundName = 'task-not-found'
      const throws = () => getTaskAssigment(taskNotFoundName, taskAssigments)
      expect(throws).toThrowError(`No tasks found for name ${taskNotFoundName}`)
    })
  })

  describe(`resolveLocalProjectConfig`, () => {
    beforeAll(() => {
      mockHarvestApiRequest(fixtureProvider.HarvestAccountId())
        .get('/users/me')
        .reply(200, fixtureProvider.HarvestUser())

      mockHarvestApiRequest(fixtureProvider.HarvestAccountId())
        .get('/users/me/project_assignments')
        .reply(200, fixtureProvider.HarvestProjectAssigmentsResponse())
    })

    it(`should resolve project config correctly`, async () => {
      const projectConfig = await resolveLocalProjectConfig(
        logger, TEST_ACCESS_TOKEN, fixtureProvider.HarvestAccountId(),
        fixtureProvider.ProjectName(), fixtureProvider.TaskName())

      expect(projectConfig).toEqual(fixtureProvider.ProjectConfig())
    })
  })
})
