import * as jsonfile from 'jsonfile'
import { IAppConfig, IProjectConfig, ISaldoConfig } from '../config'
import { IConsoleLogger } from '../utils/logger'

export function writeLocalConfigFile(
  logger: IConsoleLogger,
  accessToken: string,
  outputFile: string,
  sourceProject: IProjectConfig,
  targetProject: IProjectConfig,
  saldoConfig: ISaldoConfig
): void {
  const newConfig: IAppConfig = {
    accessToken,
    sourceProject,
    targetProject,
    saldo: saldoConfig
  }

  jsonfile.writeFileSync(outputFile, newConfig, { spaces: 2 })
  logger.message(`Config saved to ${outputFile}`)
  logger.verbose('created config file', newConfig)
}
