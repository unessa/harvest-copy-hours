import { readCommandLineArgs, getUsageGuide } from './command-line-args'
import { resolveLocalProjectConfig } from './resolve-project-config'
import { writeLocalConfigFile } from './write-config-file'
import { configureLogger, LogLevel } from '../utils/logger'
import { ISaldoConfig } from '../config'

const logger = configureLogger(LogLevel.Verbose)

async function main(): Promise<void> {
  try {
    const options = readCommandLineArgs()

    if (options.help) {
      logger.message(getUsageGuide())
      return
    }

    const sourceProject = await resolveLocalProjectConfig(
      logger,
      options['access-token'],
      options['source-harvest-account-id'],
      options['source-project-name'],
      options['source-task-name']
    )

    const targetProject = await resolveLocalProjectConfig(
      logger,
      options['access-token'],
      options['target-harvest-account-id'],
      options['target-project-name'],
      options['target-task-name']
    )

    const saldoConfig: ISaldoConfig = {
      fromDate: options['saldo-from-date'],
      toDate: options['saldo-to-date'],
      hoursPerDay: options['saldo-hours-per-day'],
      saldoProjectId: options['saldo-hours-project-id'],
      saldoTaskId: options['saldo-hours-task-id']
    }

    writeLocalConfigFile(
      logger,
      options['access-token'],
      options['output'],
      sourceProject,
      targetProject,
      saldoConfig
    )
  } catch (err) {
    logger.error(err.message)
  }
}

main()
